package com.example.testbitware.ui.modules.examenres.interfaces;

import com.example.testbitware.data.models.ResponseGet;

public interface IListenerAdapterClientes {
    void editarCliente(final ResponseGet data);
}
