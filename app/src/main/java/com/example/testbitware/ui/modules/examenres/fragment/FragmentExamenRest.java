package com.example.testbitware.ui.modules.examenres.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testbitware.R;
import com.example.testbitware.data.RetrofitClient;
import com.example.testbitware.data.models.ErrorResponse;
import com.example.testbitware.data.models.RequestPost;
import com.example.testbitware.data.models.RequestPut;
import com.example.testbitware.data.models.ResponseGet;
import com.example.testbitware.ui.modules.examenres.adapter.ClientesAdapter;
import com.example.testbitware.ui.modules.examenres.interfaces.IListenerAdapterClientes;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentExamenRest extends Fragment implements View.OnClickListener, IListenerAdapterClientes {


    /**
     * Progress dialog
     */
    ProgressDialog progressDialog;
    /**
     * Recursos principal
     **/
    private Button btnListaClientes;
    private Button btnAgregarCliente;
    private ConstraintLayout clContenedorFondoImegenes;
    private ConstraintLayout clContenedorListaClientes;
    private ConstraintLayout clContenedorAgregarCliente;
    private ConstraintLayout clContenedorEditarCliente;
    private List<ResponseGet> listClientes;

    /**
     * Recursos lista clientes
     **/
    private RecyclerView rvListaClientes;
    private ClientesAdapter clientesAdapter;
    private LinearLayoutManager layoutManagerClientesAdapter;

    /**
     * Recursos agregar cliente
     **/
    private EditText etNombreAgregar;
    private EditText etApellidosAgregar;
    private EditText etUsuarioAgregar;
    private EditText etEmailAgregar;
    private EditText etPasswordAgregar;
    private Button btnRegistrarCliente;

    /**
     * Recursos editar cliente
     **/
    private EditText etEdadEditar;
    private EditText etEstaturaEditar;
    private EditText etPesoEditar;
    private EditText etGEBEditar;
    private Button btnEditarCliente;


    private RetrofitClient retrofitClient;

    private List<ErrorResponse> errorResponse;

    private int idCliente;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_examen_rest, container, false);

        inicializarElementos();

        btnListaClientes = view.findViewById(R.id.btnListaClientes);
        btnAgregarCliente = view.findViewById(R.id.btnAgregarCliente);
        clContenedorFondoImegenes = view.findViewById(R.id.clContenedorFondoImegenes);
        clContenedorListaClientes = view.findViewById(R.id.clContenedorListaClientes);
        clContenedorAgregarCliente = view.findViewById(R.id.clContenedorAgregarCliente);
        clContenedorEditarCliente = view.findViewById(R.id.clContenedorEditarCliente);

        /** recursos lista clientes **/
        rvListaClientes = view.findViewById(R.id.rvListaClientes);

        /** recursos registra cliente**/
        etNombreAgregar = view.findViewById(R.id.etNombreAgregar);
        etApellidosAgregar = view.findViewById(R.id.etApellidosAgregar);
        etUsuarioAgregar = view.findViewById(R.id.etUsuarioAgregar);
        etEmailAgregar = view.findViewById(R.id.etEmailAgregar);
        etPasswordAgregar = view.findViewById(R.id.etPasswordAgregar);
        btnRegistrarCliente = view.findViewById(R.id.btnRegistrarCliente);

        /** recursos editar cliente**/
        etEdadEditar = view.findViewById(R.id.etEdadEditar);
        etEstaturaEditar = view.findViewById(R.id.etEstaturaEditar);
        etPesoEditar = view.findViewById(R.id.etPesoEditar);
        etGEBEditar = view.findViewById(R.id.etGEBEditar);
        btnEditarCliente = view.findViewById(R.id.btnEditarCliente);

        /** listeners **/
        btnListaClientes.setOnClickListener(this);
        btnAgregarCliente.setOnClickListener(this);
        btnRegistrarCliente.setOnClickListener(this);
        btnEditarCliente.setOnClickListener(this);

        return view;
    }

    private void inicializarElementos() {

        retrofitClient = RetrofitClient.getInstance(getActivity());
        /** Inicializar progress dialog **/
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando....");
        progressDialog.setCancelable(false);

        layoutManagerClientesAdapter = new LinearLayoutManager(getActivity());
        layoutManagerClientesAdapter.setOrientation(LinearLayoutManager.VERTICAL);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnListaClientes:
                clContenedorListaClientes.setVisibility(View.VISIBLE);
                clContenedorAgregarCliente.setVisibility(View.GONE);
                clContenedorEditarCliente.setVisibility(View.GONE);
                clContenedorFondoImegenes.setVisibility(View.GONE);

                /*** ejecutar servicio para traer clientes **/
                ejecutarServicioGetCliente();
                break;
            case R.id.btnAgregarCliente:
                clContenedorListaClientes.setVisibility(View.GONE);
                clContenedorAgregarCliente.setVisibility(View.VISIBLE);
                clContenedorEditarCliente.setVisibility(View.GONE);
                clContenedorFondoImegenes.setVisibility(View.GONE);
                break;
            case R.id.btnRegistrarCliente:
                if (!validarDatos()) {
                    registrarUsuario();
                } else {
                    completeCampos();
                }
                break;
            case R.id.btnEditarCliente:
                if (!validarDatos()) {
                    actualizarUsuario();
                } else {
                    completeCampos();
                }
                break;
        }
    }

    private void completeCampos() {
        new AlertDialog.Builder(getActivity()).setTitle(getActivity().getString(R.string.atencion))
                .setCancelable(false)
                .setMessage("Complete todos los campos")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void ejecutarServicioGetCliente() {
        progressDialog.show();
//        GetDataService service = RetrofitClient.getRetrofitInstance().create(GetDataService.class);
//        Call<JsonArray> call = service.getDataResponse();
        Call<JsonArray> callJsonArray = retrofitClient.getDataService().getDataResponse();
        callJsonArray.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        listClientes = new Gson().fromJson(response.body().toString(), new TypeToken<ArrayList<ResponseGet>>() {
                        }.getType());

                        mostrarListaClientes(listClientes);
                    }
                } else {
                    Toast.makeText(getActivity(), "Response Error: HTTP Code " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressDialog.dismiss();
                if (t instanceof IOException) {
                    Toast.makeText(getActivity(), "Network error", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "conversion issue error!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void mostrarListaClientes(final List<ResponseGet> listaClientes) {
        clientesAdapter = new ClientesAdapter(getActivity(), listClientes, this);
        rvListaClientes.setAdapter(clientesAdapter);
        rvListaClientes.setLayoutManager(layoutManagerClientesAdapter);
    }

    @Override
    public void editarCliente(final ResponseGet data) {
        new AlertDialog.Builder(getActivity()).setTitle(getActivity().getString(R.string.atencion))
                .setCancelable(false)
                .setMessage("¿Deseas editar el usuario " + data.getNombre() + " ?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        editarUsuario(data);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void editarUsuario(final ResponseGet data) {
        idCliente = data.getCliente_ID();
        clContenedorListaClientes.setVisibility(View.GONE);
        clContenedorAgregarCliente.setVisibility(View.GONE);
        clContenedorEditarCliente.setVisibility(View.VISIBLE);
        clContenedorFondoImegenes.setVisibility(View.GONE);

        /** Llenar campos usuario de editar ***/
        etEdadEditar.setText(data.getEdad() + " años");
        etEstaturaEditar.setText(data.getEstatura() + " m");
        etPesoEditar.setText(data.getPeso() + " kg");
        etGEBEditar.setText(data.getGEB() + "");
    }

    private boolean validarDatos() {
        if (etEdadEditar.getText().toString().isEmpty()) {
            return true;
        } else if (etEstaturaEditar.getText().toString().isEmpty()) {
            return true;
        } else if (etPesoEditar.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private void registrarUsuario() {
        progressDialog.show();
        RequestPost requestPost = new RequestPost();
        requestPost.setNombre(etNombreAgregar.getText().toString().trim());
        requestPost.setApellidos(etApellidosAgregar.getText().toString().trim());
        requestPost.setNombre_Usuario(etUsuarioAgregar.getText().toString().trim());
        requestPost.setCorreo_Electronico(etEmailAgregar.getText().toString().trim());
        requestPost.setContraseña(etPasswordAgregar.getText().toString().trim());

        Call<JsonArray> jsonObjectCall;
        jsonObjectCall = retrofitClient.getDataService().postDataResponse(requestPost);
        jsonObjectCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(getActivity(), "Respuesta exitosa" + response.body(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    errorResponse = new Gson().fromJson(response.errorBody().charStream(), new TypeToken<ArrayList<ErrorResponse>>() {
                    }.getType());
                    mostrarErrorServicio(errorResponse);
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressDialog.dismiss();
                if (t instanceof IOException) {
                    Toast.makeText(getActivity(), "Network error", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "conversion issue error!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void mostrarErrorServicio(final List<ErrorResponse> errorResponse) {
        new AlertDialog.Builder(getActivity()).setTitle(getActivity().getString(R.string.atencion))
                .setCancelable(false)
                .setMessage(errorResponse.get(0).getMensaje() + "")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void actualizarUsuario() {
        progressDialog.show();
        RequestPut requestPut = new RequestPut();
        requestPut.setEdad(etEdadEditar.getText().toString().trim());
        requestPut.setEstatura(etEstaturaEditar.getText().toString().trim());
        requestPut.setPeso(etPesoEditar.getText().toString().trim());
        requestPut.setGEB(etGEBEditar.getText().toString().trim());

        Call<JsonArray> jsonObjectCall;
        jsonObjectCall = retrofitClient.getDataService().putDataResponse(idCliente, requestPut);
        jsonObjectCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(getActivity(), "Respuesta exitosa" + response.body(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    errorResponse = new Gson().fromJson(response.errorBody().charStream(), new TypeToken<ArrayList<ErrorResponse>>() {
                    }.getType());
                    mostrarErrorServicio(errorResponse);
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                progressDialog.dismiss();
                if (t instanceof IOException) {
                    Toast.makeText(getActivity(), "Network error", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "conversion issue error!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
