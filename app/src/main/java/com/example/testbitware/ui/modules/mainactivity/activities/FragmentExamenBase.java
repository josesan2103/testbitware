package com.example.testbitware.ui.modules.mainactivity.activities;

import android.app.AlertDialog;
import android.app.Person;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.testbitware.R;
import com.example.testbitware.data.models.Persona;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentExamenBase extends Fragment implements View.OnClickListener {

    /**
     * Progress dialog
     */
    ProgressDialog progressDialog;
    /**
     * Recursos
     */
    private EditText etNombre;
    private EditText etEdad;
    private CheckBox cbMasculino;
    private CheckBox cbFemenino;
    private EditText etPeso;
    private EditText etAltura;
    private Button btnCalcular;
    private String sexo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        /** Enlazar recursos **/
        etNombre = view.findViewById(R.id.etNombre);
        etEdad = view.findViewById(R.id.etEdad);
        cbMasculino = view.findViewById(R.id.cbMasculino);
        cbFemenino = view.findViewById(R.id.cbFemenino);
        etPeso = view.findViewById(R.id.etPeso);
        etAltura = view.findViewById(R.id.etAltura);
        btnCalcular = view.findViewById(R.id.btnCalcular);

        /** listeners **/
        cbMasculino.setOnClickListener(this);
        cbFemenino.setOnClickListener(this);
        btnCalcular.setOnClickListener(this);

        inicializarElementos();

        return view;
    }

    private void inicializarElementos() {
        /** Inicializar progress dialog **/
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando....");
        progressDialog.setCancelable(false);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnCalcular:
                calcularPersona();
                break;
            case R.id.cbMasculino:
                sexo = "M";
                cbFemenino.setChecked(false);
                break;
            case R.id.cbFemenino:
                sexo = "F";
                cbMasculino.setChecked(false);
                break;
        }

        if (intent != null) {
            startActivity(intent);
        }
    }

    private boolean validarDatos() {
        if (etNombre.getText().toString().isEmpty()) {
            return false;
        } else if (etPeso.getText().toString().isEmpty()) {
            return false;
        } else if (etPeso.getText().toString().isEmpty()) {
            return false;
        } else if (etAltura.getText().toString().isEmpty()) {
            return false;
        } else if (!cbMasculino.isChecked() && !cbFemenino.isChecked()){
            return false;
        } else {
            return true;
        }
    }

    private void calcularPersona() {
        if (validarDatos()) {
            calcularDatos();
        } else {
            completeCampos();
        }
    }

    private void completeCampos() {
        new AlertDialog.Builder(getActivity()).setTitle(getActivity().getString(R.string.atencion))
                .setCancelable(false)
                .setMessage("Complete todos los campos")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void calcularDatos() {
        Persona persona = new Persona();
        persona.setNombre(etNombre.getText().toString().trim());
        persona.setEdad(Integer.parseInt(etEdad.getText().toString().trim()));
        persona.setSexo(sexo);
        persona.setPeso(Double.parseDouble(etPeso.getText().toString().trim()));
        persona.setAltura(Double.parseDouble(etAltura.getText().toString().trim()));

        String msjIMC = "";
        int intIMS = persona.calcularIMC();
        switch (intIMS){
            case -1:
                msjIMC = "Debajo de su peso ideal";
                break;
            case 0:
                msjIMC = "Esta en su peso ideal";
                break;
            case 1:
                msjIMC = "Tiene sobrepeso";
                break;
        }

        String esMayorEdad;
        if (persona.esMayorDeEdad()){
            esMayorEdad = "Es mayor de edad";
        } else {
            esMayorEdad = "Es menor de edad";
        }

        mostrarInfromacionUsuario(getLayoutInflater(), etNombre.getText().toString().trim(), msjIMC, esMayorEdad, persona);
    }

    public void mostrarInfromacionUsuario(LayoutInflater inflater, String nombre, String msjIMC, String esMayorEdad, Persona persona) {
        View view = inflater.inflate(R.layout.alert_datos_persona, null);
        TextView tvNombreUsuario = view.findViewById(R.id.tvNombreUsuario);
        TextView tvPesoIdealD = view.findViewById(R.id.tvPesoIdealD);
        TextView tvMayorEdadD = view.findViewById(R.id.tvMayorEdadD);
        TextView tvInformacionUsuarioD = view.findViewById(R.id.tvInformacionUsuarioD);

        Button btnAceptar = view.findViewById(R.id.btnAceptar);

        tvNombreUsuario.setText(nombre);
        tvPesoIdealD.setText(msjIMC);
        tvMayorEdadD.setText(esMayorEdad);
        tvInformacionUsuarioD.setText(persona.toString());

        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setCancelable(false)
                .create();

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
