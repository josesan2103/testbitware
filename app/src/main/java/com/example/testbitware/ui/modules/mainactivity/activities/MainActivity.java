package com.example.testbitware.ui.modules.mainactivity.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.testbitware.R;
import com.example.testbitware.ui.modules.examenres.fragment.FragmentExamenRest;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        drawerLayout = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                MainActivity.this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);

        /** listener para navigation view **/
        navigationView.setNavigationItemSelectedListener(this);
        toggle.syncState();

        /** se carga el fragment al comienzo de la actividad */
        Fragment fragExamenBase = new FragmentExamenBase();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragExamenBase); // replace a Fragment with Frame Layout
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment frag = null;
        switch (menuItem.getItemId()) {
            case R.id.nav_examen_base:
                frag = new FragmentExamenBase();
                break;
            case R.id.nav_examen_rest:
                frag = new FragmentExamenRest();
                break;
            default:
                throw new IllegalArgumentException("menu option not implemented!!");
        }

        /** se reemplaza el fragment de la opcion seleccionada en el menu drawer **/
        if (frag != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.nav_host_fragment, frag); // replace a Fragment with Frame Layout
            transaction.commit(); // commit the changes
            drawerLayout.closeDrawer(GravityCompat.START); // close the all open Drawer Views
            return true;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
