package com.example.testbitware.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.testbitware.R;
import com.example.testbitware.utils.Utileria;

/**
 * Created by jose sanchez.
 */

public class ProgressDialogInvisible extends Dialog {
    private Context context;

    public ProgressDialogInvisible(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_progress_dialog_invisible);
        if(getWindow() != null){
            getWindow().setBackgroundDrawableResource(R.color.colorInvisible);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void dismiss() {
        if (Utileria.isActivityRunning(context))
            super.dismiss();
    }
}
