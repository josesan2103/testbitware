package com.example.testbitware.ui.modules.examenres.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testbitware.R;
import com.example.testbitware.data.models.ResponseGet;
import com.example.testbitware.ui.modules.examenres.interfaces.IListenerAdapterClientes;

import java.util.List;

public class ClientesAdapter extends RecyclerView.Adapter<ClientesAdapter.ViewHolder> {

    private Context context;
    private List<ResponseGet> listClientes;
    private IListenerAdapterClientes listener;

    public ClientesAdapter(Context context, List<ResponseGet> listClientes, IListenerAdapterClientes listener) {
        this.context = context;
        this.listClientes = listClientes;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ClientesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_clientes, parent, false);

        return new ClientesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientesAdapter.ViewHolder holder, int position) {
        final ResponseGet data = listClientes.get(position);
        holder.setIsRecyclable(false);
        if (!data.getNombre().isEmpty() && !data.getApellidos().isEmpty()){
            holder.tvNombreUsuarioServicio.setText(" " + data.getNombre());
            holder.tvApellidosServicio.setText(data.getApellidos());
            holder.tvEdadServicio.setText(data.getEdad() + " años");
        }

        holder.ivEditarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.editarCliente(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listClientes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNombreUsuarioServicio, tvApellidosServicio, tvEdadServicio;
        private Button ivEditarUsuario;

        public ViewHolder(View v) {
            super(v);
            tvNombreUsuarioServicio = v.findViewById(R.id.tvNombreUsuarioServicio);
            tvApellidosServicio = v.findViewById(R.id.tvApellidosServicio);
            tvEdadServicio = v.findViewById(R.id.tvEdadServicio);
            ivEditarUsuario = v.findViewById(R.id.ivEditarUsuario);
        }
    }
}
