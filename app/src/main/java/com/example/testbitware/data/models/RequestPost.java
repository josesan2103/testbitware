package com.example.testbitware.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestPost {

    @SerializedName("Nombre")
    @Expose
    private String Nombre;
    @SerializedName("Apellidos")
    @Expose
    private String Apellidos;
    @SerializedName("Nombre_Usuario")
    @Expose
    private String Nombre_Usuario;
    @SerializedName("Correo_Electronico")
    @Expose
    private String Correo_Electronico;
    @SerializedName("Contraseña")
    @Expose
    private String Contraseña;

    public RequestPost() {
        // empty construcntor
    }

    public RequestPost(String nombre, String apellidos, String nombre_Usuario, String correo_Electronico, String contraseña) {
        Nombre = nombre;
        Apellidos = apellidos;
        Nombre_Usuario = nombre_Usuario;
        Correo_Electronico = correo_Electronico;
        Contraseña = contraseña;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getNombre_Usuario() {
        return Nombre_Usuario;
    }

    public void setNombre_Usuario(String nombre_Usuario) {
        Nombre_Usuario = nombre_Usuario;
    }

    public String getCorreo_Electronico() {
        return Correo_Electronico;
    }

    public void setCorreo_Electronico(String correo_Electronico) {
        Correo_Electronico = correo_Electronico;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String contraseña) {
        Contraseña = contraseña;
    }
}
