package com.example.testbitware.data;

import com.example.testbitware.data.models.RequestPost;
import com.example.testbitware.data.models.RequestPut;
import com.google.gson.JsonArray;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * interface for gat data service
 *
 * @author jose sanchez rosales
 */
public interface GetDataService {

    /**
     * For get service
     *
     * @return
     */
    @GET("NutriNET/Cliente")
    Call<JsonArray> getDataResponse();

    /**
     * For post service
     *
     * @return
     */
//    @FormUrlEncoded
//    @POST("NutriNET/Cliente")
//    Call<JsonArray> postDataResponse(
//            @Field("Nombre") String nombre,
//            @Field("Apellidos") String apellidos,
//            @Field("Nombre_Usuario") String nombreUsuario,
//            @Field("Correo_Electronico") String correoElectronico,
//            @Field("Contraseña") String contrasenia
//    );

    /**
     * For post service
     *
     * @return
     */
    @POST("NutriNET/Cliente")
    Call<JsonArray> postDataResponse(
            @Body RequestPost requestPost
    );

    /**
     * For put service
     *
     * @return
     */
    @PUT("NutriNET/Cliente/{idCliente}")
    Call<JsonArray> putDataResponse(
            @Path("idCliente") int idCliente,
            @Body RequestPut requestPut
    );

}
