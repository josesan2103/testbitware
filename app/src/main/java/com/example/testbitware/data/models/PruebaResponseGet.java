package com.example.testbitware.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Class for model Prueba GET
 *
 * @author jose sanchez rosales
 */
public class PruebaResponseGet {

    @SerializedName("data")
    private PruebaResponseGetOut data;
    @SerializedName("msg")
    private String msg;
    @SerializedName("code")
    private int code;

    /**
     * Empty constructor
     */
    public PruebaResponseGet() {
        // empty constructor
    }

    /**
     * Full constructor
     * @param data
     * @param msg
     * @param code
     */
    public PruebaResponseGet(PruebaResponseGetOut data, String msg, int code) {
        this.data = data;
        this.msg = msg;
        this.code = code;
    }

    /**
     * get Object data
     * @return
     */
    public PruebaResponseGetOut getData() {
        return data;
    }

    /**
     * set object data
     * @param data
     */
    public void setData(PruebaResponseGetOut data) {
        this.data = data;
    }

    /**
     * get message
     * @return
     */
    public String getMsg() {
        return msg;
    }

    /**
     * set message
     * @param msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * get code
     * @return
     */
    public int getCode() {
        return code;
    }

    /**
     * set code
     * @param code
     */
    public void setCode(int code) {
        this.code = code;
    }
}
