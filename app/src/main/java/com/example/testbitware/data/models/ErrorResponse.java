package com.example.testbitware.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    @SerializedName("Cve_Mensaje")
    private int Cve_Mensaje;
    @SerializedName("Mensaje")
    private String Mensaje;

    public ErrorResponse() {
        // empty constructor
    }

    public ErrorResponse(int cve_Mensaje, String mensaje) {
        Cve_Mensaje = cve_Mensaje;
        Mensaje = mensaje;
    }

    public int getCve_Mensaje() {
        return Cve_Mensaje;
    }

    public void setCve_Mensaje(int cve_Mensaje) {
        Cve_Mensaje = cve_Mensaje;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }
}
