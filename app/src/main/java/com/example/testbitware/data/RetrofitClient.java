package com.example.testbitware.data;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String BASE_URL = "http://187.188.122.85:8091/";
    private static Retrofit retrofit;
    private static RetrofitClient instance = null;
    private static OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    private static OkHttpClient client = new OkHttpClient();
    private static Retrofit.Builder builder;
    public Context context;
    private GetDataService getDataService;

    private RetrofitClient(Context context) {
        buildService(context);
    }

    public static synchronized RetrofitClient getInstance(Context contex) {
        if (instance == null) {
            instance = new RetrofitClient(contex);
        }

        return instance;
    }

    private void buildService(Context context) {
        client = httpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Log.i("Url servicio: %s", original.url().toString());
                        Request.Builder builder = original.newBuilder()
                                .method(original.method(), original.body());
                        return chain.proceed(builder.build());
                    }
                })
                .build();
        builder = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL);

        retrofit = builder.build();
        getDataService = retrofit.create(GetDataService.class);
    }

    public GetDataService getDataService() {
        return getDataService;
    }

    public void setGetDataService(GetDataService getDataService) {
        this.getDataService = getDataService;
    }
}
