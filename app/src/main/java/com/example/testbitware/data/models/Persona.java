package com.example.testbitware.data.models;

import java.util.Random;

public class Persona {
    private int idPersona = 0;
    private String nombre = "";
    private int edad = 0;
    private String NSS;
    private String sexo = "M";
    private double peso = 0.0;
    private double altura = 0.0;

    public Persona() {
        // empty constructor
        generaNSS();
    }

    public Persona(int idPersona, String nombre, int edad, String NSS, String sexo, double peso, double altura) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.edad = edad;
        this.NSS = NSS;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
        generaNSS();
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNSS() {
        return NSS;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public int calcularIMC() {
        int auxIMC = 2;
        double imc = peso / (Math.pow(altura, 2));
        if (sexo.equals("M")) {
            if (imc < 20) {
                auxIMC = -1;
            } else if (imc >= 20 && imc <= 25) {
                auxIMC = 0;
            } else {
                auxIMC = 1;
            }
        } else {
            if (imc < 19) {
                auxIMC = -1;
            } else if (imc >= 19 && imc <= 24) {
                auxIMC = 0;
            } else {
                auxIMC = 1;
            }
        }
        return auxIMC;
    }

    @Override
    public String toString() {
        return "Nombre=" + nombre + "," + '\n' +
                " edad=" + edad + "," + '\n' +
                " NSS=" + NSS + "," + '\n' +
                " sexo=" + sexo + "," + '\n' +
                " peso=" + peso + "," + '\n' +
                " altura=" + altura;
    }

    public boolean esMayorDeEdad() {
        if (edad >= 18) {
            return true;
        } else {
            return false;
        }
    }

    private void generaNSS() {
        char [] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
        int charsLength = chars.length;
        Random random = new Random();
        StringBuffer buffer = new StringBuffer();
        for (int i=0;i<8;i++){
            buffer.append(chars[random.nextInt(charsLength)]);
        }

        this.NSS = buffer.toString();
    }

}
