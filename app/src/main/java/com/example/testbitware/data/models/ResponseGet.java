package com.example.testbitware.data.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class ResponseGet implements Serializable {

    @SerializedName("Cliente_ID")
    private int Cliente_ID;
    @SerializedName("Nombre_Usuario")
    private String Nombre_Usuario;
    @SerializedName("Contraseña")
    private String Contraseña;
    @SerializedName("Nombre")
    private String Nombre;
    @SerializedName("Apellidos")
    private String Apellidos;
    @SerializedName("Correo_Electronico")
    private String Correo_Electronico;
    @SerializedName("Edad")
    private int Edad;
    @SerializedName("Estatura")
    private double Estatura;
    @SerializedName("Peso")
    private double Peso;
    @SerializedName("Genero_ID")
    private int Genero_ID;
    @SerializedName("Actividad_Fisica_ID")
    private int Actividad_Fisica_ID;
    @SerializedName("Dieta_ID")
    private int Dieta_ID;
    @SerializedName("Objetivo_ID")
    private int Objetivo_ID;
    @SerializedName("IMC")
    private double IMC;
    @SerializedName("GEB")
    private double GEB;
    @SerializedName("ETA")
    private double ETA;
    @SerializedName("Peso_Maximo")
    private double Peso_Maximo;
    @SerializedName("Peso_Minimo")
    private double Peso_Minimo;
    @SerializedName("AF")
    private double AF;
    @SerializedName("Gasto_Energetico_Total")
    private double Gasto_Energetico_Total;
    @SerializedName("Tipo_Cliente_ID")
    private int Tipo_Cliente_ID;
    @SerializedName("Activo")
    private boolean Activo;
    @SerializedName("Orden")
    private int Orden;
    @SerializedName("Fecha_Creacion")
    private String fecha_Creacion;
    @SerializedName("Fecha_Actualizacion")
    private String fecha_Actualizacion;
    @SerializedName("Usuario_ID")
    private int Usuario_ID;
    @SerializedName("Visible")
    private boolean Visible;
    @SerializedName("De_Sistema")
    private boolean De_Sistema;

    public ResponseGet() {
        // empty constructor
    }

    public ResponseGet(int cliente_ID, String nombre_Usuario, String contraseña, String nombre, String apellidos, String correo_Electronico, int edad, double estatura, double peso, int genero_ID, int actividad_Fisica_ID, int dieta_ID, int objetivo_ID, double IMC, double GEB, double ETA, double peso_Maximo, double peso_Minimo, double AF, double gasto_Energetico_Total, int tipo_Cliente_ID, boolean activo, int orden, String fecha_Creacion, String fecha_Actualizacion, int usuario_ID, boolean visible, boolean de_Sistema) {
        Cliente_ID = cliente_ID;
        Nombre_Usuario = nombre_Usuario;
        Contraseña = contraseña;
        Nombre = nombre;
        Apellidos = apellidos;
        Correo_Electronico = correo_Electronico;
        Edad = edad;
        Estatura = estatura;
        Peso = peso;
        Genero_ID = genero_ID;
        Actividad_Fisica_ID = actividad_Fisica_ID;
        Dieta_ID = dieta_ID;
        Objetivo_ID = objetivo_ID;
        this.IMC = IMC;
        this.GEB = GEB;
        this.ETA = ETA;
        Peso_Maximo = peso_Maximo;
        Peso_Minimo = peso_Minimo;
        this.AF = AF;
        Gasto_Energetico_Total = gasto_Energetico_Total;
        Tipo_Cliente_ID = tipo_Cliente_ID;
        Activo = activo;
        Orden = orden;
        fecha_Creacion = fecha_Creacion;
        fecha_Actualizacion = fecha_Actualizacion;
        Usuario_ID = usuario_ID;
        Visible = visible;
        De_Sistema = de_Sistema;
    }

    public int getCliente_ID() {
        return Cliente_ID;
    }

    public void setCliente_ID(int cliente_ID) {
        Cliente_ID = cliente_ID;
    }

    public String getNombre_Usuario() {
        return Nombre_Usuario;
    }

    public void setNombre_Usuario(String nombre_Usuario) {
        Nombre_Usuario = nombre_Usuario;
    }

    public String getContraseña() {
        return Contraseña;
    }

    public void setContraseña(String contraseña) {
        Contraseña = contraseña;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getCorreo_Electronico() {
        return Correo_Electronico;
    }

    public void setCorreo_Electronico(String correo_Electronico) {
        Correo_Electronico = correo_Electronico;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int edad) {
        Edad = edad;
    }

    public double getEstatura() {
        return Estatura;
    }

    public void setEstatura(double estatura) {
        Estatura = estatura;
    }

    public double getPeso() {
        return Peso;
    }

    public void setPeso(double peso) {
        Peso = peso;
    }

    public int getGenero_ID() {
        return Genero_ID;
    }

    public void setGenero_ID(int genero_ID) {
        Genero_ID = genero_ID;
    }

    public int getActividad_Fisica_ID() {
        return Actividad_Fisica_ID;
    }

    public void setActividad_Fisica_ID(int actividad_Fisica_ID) {
        Actividad_Fisica_ID = actividad_Fisica_ID;
    }

    public int getDieta_ID() {
        return Dieta_ID;
    }

    public void setDieta_ID(int dieta_ID) {
        Dieta_ID = dieta_ID;
    }

    public int getObjetivo_ID() {
        return Objetivo_ID;
    }

    public void setObjetivo_ID(int objetivo_ID) {
        Objetivo_ID = objetivo_ID;
    }

    public double getIMC() {
        return IMC;
    }

    public void setIMC(double IMC) {
        this.IMC = IMC;
    }

    public double getGEB() {
        return GEB;
    }

    public void setGEB(double GEB) {
        this.GEB = GEB;
    }

    public double getETA() {
        return ETA;
    }

    public void setETA(double ETA) {
        this.ETA = ETA;
    }

    public double getPeso_Maximo() {
        return Peso_Maximo;
    }

    public void setPeso_Maximo(double peso_Maximo) {
        Peso_Maximo = peso_Maximo;
    }

    public double getPeso_Minimo() {
        return Peso_Minimo;
    }

    public void setPeso_Minimo(double peso_Minimo) {
        Peso_Minimo = peso_Minimo;
    }

    public double getAF() {
        return AF;
    }

    public void setAF(double AF) {
        this.AF = AF;
    }

    public double getGasto_Energetico_Total() {
        return Gasto_Energetico_Total;
    }

    public void setGasto_Energetico_Total(double gasto_Energetico_Total) {
        Gasto_Energetico_Total = gasto_Energetico_Total;
    }

    public int getTipo_Cliente_ID() {
        return Tipo_Cliente_ID;
    }

    public void setTipo_Cliente_ID(int tipo_Cliente_ID) {
        Tipo_Cliente_ID = tipo_Cliente_ID;
    }

    public boolean isActivo() {
        return Activo;
    }

    public void setActivo(boolean activo) {
        Activo = activo;
    }

    public int getOrden() {
        return Orden;
    }

    public void setOrden(int orden) {
        Orden = orden;
    }

    public String getFecha_Creacion() {
        return fecha_Creacion;
    }

    public void setFecha_Creacion(String fecha_Creacion) {
        fecha_Creacion = fecha_Creacion;
    }

    public String getFecha_Actualizacion() {
        return fecha_Actualizacion;
    }

    public void setFecha_Actualizacion(String fecha_Actualizacion) {
        fecha_Actualizacion = fecha_Actualizacion;
    }

    public int getUsuario_ID() {
        return Usuario_ID;
    }

    public void setUsuario_ID(int usuario_ID) {
        Usuario_ID = usuario_ID;
    }

    public boolean isVisible() {
        return Visible;
    }

    public void setVisible(boolean visible) {
        Visible = visible;
    }

    public boolean isDe_Sistema() {
        return De_Sistema;
    }

    public void setDe_Sistema(boolean de_Sistema) {
        De_Sistema = de_Sistema;
    }
}
