package com.example.testbitware.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Class for model ObjetoPrueba
 *
 * @author jose sanchez rosales
 */
public class PruebaResponseGetOut {

    @SerializedName("dato1")
    private int dato1;
    @SerializedName("dato2")
    private boolean dato2;
    @SerializedName("dato3")
    private double dato3;

    /**
     * Empty constructor
     */
    public PruebaResponseGetOut() {
        // empty constructor
    }

    /**
     * Full Constructor
     * @param dato1
     * @param dato2
     * @param dato3
     */
    public PruebaResponseGetOut(int dato1, boolean dato2, double dato3) {
        this.dato1 = dato1;
        this.dato2 = dato2;
        this.dato3 = dato3;
    }

    /**
     * get dato1
     * @return
     */
    public int getDato1() {
        return dato1;
    }

    /**
     * set dato 1
     * @param dato1
     */
    public void setDato1(int dato1) {
        this.dato1 = dato1;
    }

    /**
     * get dato 2
     * @return
     */
    public boolean getDato2() {
        return dato2;
    }

    /**
     * set dato 2
     * @param dato2
     */
    public void setDato2(boolean dato2) {
        this.dato2 = dato2;
    }

    /**
     * get dato 3
     * @return
     */
    public double getDato3() {
        return dato3;
    }

    /**
     * set dato 3
     * @param dato3
     */
    public void setDato3(double dato3) {
        this.dato3 = dato3;
    }
}
