package com.example.testbitware.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestPut {

    @SerializedName("Edad")
    @Expose
    private String Edad;
    @SerializedName("Estatura")
    @Expose
    private String Estatura;
    @SerializedName("Peso")
    @Expose
    private String Peso;
    @SerializedName("GEB")
    @Expose
    private String GEB;

    public RequestPut() {
        // empty construcntor
    }

    public RequestPut(String edad, String estatura, String peso, String GEB) {
        Edad = edad;
        Estatura = estatura;
        Peso = peso;
        this.GEB = GEB;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String edad) {
        Edad = edad;
    }

    public String getEstatura() {
        return Estatura;
    }

    public void setEstatura(String estatura) {
        Estatura = estatura;
    }

    public String getPeso() {
        return Peso;
    }

    public void setPeso(String peso) {
        Peso = peso;
    }

    public String getGEB() {
        return GEB;
    }

    public void setGEB(String GEB) {
        this.GEB = GEB;
    }
}
