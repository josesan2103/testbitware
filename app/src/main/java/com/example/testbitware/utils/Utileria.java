package com.example.testbitware.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class Utileria {

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isActivityRunning(Context context) {
        Activity activity = (Activity) context;
        if (activity != null) {
            return !activity.isDestroyed() || !activity.isFinishing();
        } else {
            return false;
        }
    }
}
